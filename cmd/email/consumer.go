package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"sync"

	"github.com/IBM/sarama"

	"email_service/internal/email"
)

const topic = "email"

var wg sync.WaitGroup

type EmailMsg struct {
	OrderID string `json:"order_id"`
	UserID  string `json:"user_id"`
}

func main() {
	sarama.Logger = log.New(os.Stdout, "[sarama]", log.LstdFlags)
	done := make(chan struct{})

	consumer, err := sarama.NewConsumer([]string{"my-cluster-kafka-bootstrap:9092"}, sarama.NewConfig())
	if err != nil {
		log.Fatal("error on sarama.NewConsumer", err)
	}

	defer func() {
		close(done)
		if err := consumer.Close(); err != nil {
			log.Println("gracefully close channel and kafka consumer", err)
		}
	}()

	partitions, err := consumer.Partitions(topic)
	if err != nil {
		log.Fatal("consumer.Partitions", err)
	}
	for i := range partitions {
		partitionConsumer, err := consumer.ConsumePartition(topic, partitions[i], sarama.OffsetNewest)
		if err != nil {
			log.Fatal("consumer.ConsumePartition", err)
		}

		defer func() {
			if err := partitionConsumer.Close(); err != nil {
				log.Println("gracefully close kafka partitionConsumer", err)
			}
		}()

		wg.Add(1)
		go awaitMessages(partitionConsumer, partitions[i], done)
	}
	wg.Wait()
}

func awaitMessages(partitionConsumer sarama.PartitionConsumer, partition int32, done chan struct{}) {
	defer wg.Done()
	for {
		select {
		case msg, ok := <-partitionConsumer.Messages():
			if !ok {
				log.Println("Channel closed, exiting")
				return
			}

			log.Printf("Partition %d - Received message: %v\n", partition, msg)
			handleMessage(msg)

		case <-done:
			fmt.Printf("Received done signal. Exiting ...\n")
			return
		}
	}
}

func handleMessage(msg *sarama.ConsumerMessage) {
	var emailMessage EmailMsg

	if err := json.Unmarshal(msg.Value, &emailMessage); err != nil {
		log.Printf("Error unmarshaling JSON: %v\n", err)
		return
	}

	if err := email.Send(emailMessage.UserID, emailMessage.OrderID); err != nil {
		log.Printf("Error email.Send: %v\n", err)
		return
	}
}

package email

import (
	"fmt"
	"log"
	"net/smtp"
	"os"
)

func Send(target string, orderID string) error {
	senderEmail := os.Getenv("SENDER_EMAIL")
	password := os.Getenv("SENDER_PASSWORD")

	recipientEmail := target

	message := []byte(fmt.Sprintf(
		"From: "+senderEmail+"\n"+
			"To: "+recipientEmail+"\n"+
			"Subject: Payment Processed!\nProcess ID: %s\n", orderID))

	smtpServer := "smtp.gmail.com"
	smtpPort := 587

	creds := smtp.PlainAuth("", senderEmail, password, smtpServer)
	smtpAddress := fmt.Sprintf("%s:%d", smtpServer, smtpPort)

	if err := smtp.SendMail(smtpAddress, creds, senderEmail, []string{recipientEmail}, message); err != nil {
		log.Println("Error on smtp.SendMail: ", err)
		return err
	}

	return nil
}
